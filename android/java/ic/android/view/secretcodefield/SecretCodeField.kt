package ic.android.view.secretcodefield


import android.content.Context
import android.graphics.Typeface
import android.text.InputType
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.KeyEvent
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.LinearLayout
import ic.android.storage.assets.getFontFromAssets

import ic.android.ui.view.ext.requestFocusAndKeyboard
import ic.android.ui.view.text.TextView
import ic.android.ui.view.ext.setOnClickAction
import ic.android.ui.view.ext.setOnLongClickAction
import ic.android.ui.view.group.ext.get
import ic.android.ui.view.text.ext.textColorArgb
import ic.android.ui.view.text.ext.value
import ic.base.primitives.int32.ext.asFloat32
import ic.base.throwables.WrongValueException
import ic.gui.secretcodefield.R


class SecretCodeField : LinearLayout {


	init {
		orientation = HORIZONTAL
	}


	private var maxLengthField : Int = 0
	var maxLength : Int
		get() = maxLengthField
		set(value) {
			maxLengthField = value
			updateStructure()
		}
	;

	private fun updateStructure() {
		removeAllViews()
		for (i in 0 until maxLengthField) {
			addView(
				TextView(context).apply {
					gravity = Gravity.CENTER
					value = "•"
				}
			)
		}
		updateDimensions()
		updateTextSize()
		updateTextFont()
		updateColors()
	}


	private var groupLengthField : Int = 1
	var groupLength : Int
		get() = groupLengthField
		set(value) {
			groupLengthField = value
			updateDimensions()
		}
	;

	private var charWidthPxField : Int = 0
	var charWidthPx : Int
		get() = charWidthPxField
		set(value) {
			charWidthPxField = value
			updateDimensions()
		}
	;

	private var groupDividerWidthPxField : Int = 0
	var groupDividerWidthPx : Int
		get() = groupDividerWidthPxField
		set(value) {
			groupDividerWidthPxField = value
			updateDimensions()
		}
	;

	private fun updateDimensions() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.layoutParams = (charView.layoutParams as LinearLayout.LayoutParams).apply {
				width = charWidthPxField
				height = MATCH_PARENT
				leftMargin = (
					if (i % groupLengthField == 0 && i != 0) {
						groupDividerWidthPxField
					} else {
						0
					}
				)
			}
		}
	}


	private var textSizePxField : Int = 0
	var textSizePx : Int
		get() = textSizePxField
		set(value) {
			textSizePxField = value
			updateTextSize()
		}
	;

	private fun updateTextSize() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizePxField.asFloat32)
		}
	}


	private var textFontField : Typeface = Typeface.DEFAULT
	var textFont : Typeface
		get() = textFontField
		set(value) {
			textFontField = value
			updateTextFont()
		}
	;

	private fun updateTextFont() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.typeface = textFontField
		}
	}


	private var textColorField : Int = 0
	var textColor : Int
		get() = textColorField
		set(value) {
			textColorField = value
			updateColors()
		}
	;

	private var hintColorField : Int = 0
	var hintColor : Int
		get() = hintColorField
		set(value) {
			hintColorField = value
			updateColors()
		}
	;


	private fun updateColors() {
		for (i in 0 until maxLengthField) {
			val charView : TextView = this[i]
			charView.textColorArgb = if (i < valueField.length) textColorField else hintColorField
		}
	}


	private var valueField : String = ""
	var value : String
		get() = valueField
		set(value) {
			val oldValue = valueField
			valueField = value
			updateColors()
			onValueChangedAction(oldValue, value)
		}
	;

	private var onValueChangedAction : (oldValue: String, newValue: String) -> Unit = { _, _ -> }

	fun setOnValueChangedAction (
		toCallAtOnce : Boolean = false,
		action : (oldValue: String, newValue: String) -> Unit
	) {
		this.onValueChangedAction = action
		if (toCallAtOnce) action(valueField, valueField)
	}


	var textType : TextType = TextType.LatinLettersAndDigits

	var isLocked : Boolean = false


	init {
		isClickable = true
		isFocusable = true
		isFocusableInTouchMode = true
		isLongClickable = true
		setOnClickAction {
			requestFocusAndKeyboard()
		}
		setOnLongClickAction {

		}
	}

	override fun onCreateInputConnection(outAttrs: EditorInfo) : InputConnection? {
		outAttrs.inputType = when (textType) {
			TextType.LatinLettersAndDigits -> (
				InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
			)
			TextType.DigitsOnly -> InputType.TYPE_CLASS_NUMBER
		}
		outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE
		return null
	}


	override fun dispatchKeyEvent (event: KeyEvent) : Boolean {
		if (event.action == KeyEvent.ACTION_DOWN) {
			if (!isLocked) {
				when {
					event.keyCode == KeyEvent.KEYCODE_DEL -> {
						if (value.length > 0) value = value.substring(0, value.length - 1)
					}
					else -> {
						if (value.length < maxLengthField) {
							val c = event.unicodeChar.toChar()
							if (textType.filterChar(c)) {
								value += c
							}
						}
					}
				}
			}
		}
		return super.dispatchKeyEvent(event)
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super (context, attrs, defStyleAttr)
	{

		if (attrs != null) {

			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.SecretCodeField)

			maxLengthField = styledAttributes.getInteger(R.styleable.SecretCodeField_maxLength, maxLengthField)

			groupLengthField = styledAttributes.getInteger(R.styleable.SecretCodeField_groupLength, groupLengthField)

			charWidthPxField = styledAttributes.getDimensionPixelSize(R.styleable.SecretCodeField_charWidth, charWidthPxField)

			groupDividerWidthPxField = (
				styledAttributes.getDimensionPixelSize(R.styleable.SecretCodeField_groupDividerWidth, groupDividerWidthPxField)
			)

			textSizePxField = styledAttributes.getDimensionPixelSize(R.styleable.SecretCodeField_textSize, textSizePxField)

			val textFontName = styledAttributes.getString(R.styleable.SecretCodeField_textFont)
			if (textFontName != null) textFontField = getFontFromAssets(textFontName)

			textColorField = styledAttributes.getColor(R.styleable.SecretCodeField_textColor, textColorField)
			hintColorField = styledAttributes.getColor(R.styleable.SecretCodeField_hintColor, hintColorField)

			valueField = styledAttributes.getString(R.styleable.SecretCodeField_value) ?: valueField

			val textTypeCode = styledAttributes.getInteger(R.styleable.SecretCodeField_textType, 0)
			if (textTypeCode != 0) {
				textType = when (textTypeCode) {
					1 -> TextType.LatinLettersAndDigits
					2 -> TextType.DigitsOnly
					else -> throw WrongValueException.Error("textTypeCode: $textTypeCode")
				}
			}

			styledAttributes.recycle()

		}

		updateStructure()

	}


}